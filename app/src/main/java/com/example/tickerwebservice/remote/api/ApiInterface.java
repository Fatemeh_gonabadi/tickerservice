package com.example.tickerwebservice.remote.api;

import com.example.tickerwebservice.remote.pojo.CurrencyPrice;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiInterface {
    @GET("ticker/price")
    public Observable<CurrencyPrice> getPrice(@Query("symbol") String symbol);
}
