package com.example.tickerwebservice.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import com.example.tickerwebservice.R;
import com.example.tickerwebservice.remote.api.ApiClient;
import com.example.tickerwebservice.remote.api.ApiInterface;
import com.example.tickerwebservice.remote.pojo.CurrencyPrice;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    public static final String BASE_URL = "https://api.binance.com/api/v3/";
    String TAG = "TAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        generateData();
    }

    public void generateData() {

        ApiInterface apiInterface = ApiClient.get().create(ApiInterface.class);
        Observable<CurrencyPrice> currencyPriceObservable = apiInterface.getPrice("BTCUSDT");

        currencyPriceObservable.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .retry(2)
                .timeout(10, TimeUnit.SECONDS).
                subscribe(new Subscriber<CurrencyPrice>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "COMPLETED");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Error", e.toString());
                    }

                    @Override
                    public void onNext(CurrencyPrice currencyPrice) {
                        Log.d(TAG, currencyPrice.getPrice() + "");
                    }
                });
    }
}